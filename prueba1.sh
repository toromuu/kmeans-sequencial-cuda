#!/bin/bash


# var n --> cte k,d

for n in 500 5000 50000 500000 1000000
do
    for k in 16
    do
        for d in 2
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Nvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Nvar.txt
            mv centroides.txt ./dataset/centroides/Nvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Nvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done

