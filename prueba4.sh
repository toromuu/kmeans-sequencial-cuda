#!/bin/bash


# var n,k,d --> cte n,k

for n in 5000 50000
do
    for k in 64 256
    do
        for d in 4 8
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda.txt
            mv centroides.txt ./dataset/centroides/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done


