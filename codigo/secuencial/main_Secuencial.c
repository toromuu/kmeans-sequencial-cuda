#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "ctimer.h"

#define MAX 5
#define MIN -5

#define	puntos(i,j)		puntos[ (i) + ((j)*(n)) ]
#define	centroides(i,j)		centroides[ (i) + ((j)*(k)) ]
#define	centroidesAux(i,j)		centroidesAux[ (i) + ((j)*(k)) ]

 // Crear puntos de manera random
void inicializarPuntos(float *puntos, int n, int d){
	int nAux,dAux;

    for(nAux=0; nAux<n; nAux++){
        for(dAux=0; dAux<d; dAux++){
            puntos(nAux, dAux) = (float)(MIN + rand() % ( MAX - MIN + 1 ));
        }
    }
} // ()

 // Mostrar los puntos por pantalla
void printearPuntos(float *puntos, int n, int d){
    int nAux,dAux;

    for(nAux=0; nAux<n; nAux++){
        printf("Punto %d: ",nAux);
        for(dAux=0; dAux<d; dAux++){
            printf("%f ", puntos(nAux, dAux));
        }
        printf("\n");
    }
} // ()

 // Importar datos de puntos.txt
float *inicializarPuntosFichero(int *n, int *d) {
	int i,j;
    float num; 
	FILE *fp;
	if((fp=fopen("puntos.txt","r"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

	if(fscanf(fp,"N=%d,D=%d\n",n, d)!=2) fprintf(stderr,"load error!\n");
    float *puntos = (float*) malloc((*n)*(*d)*sizeof(float*));
	for(i=0;i<*n;i++){
        for(j=0; j<*d; j++){
            fscanf (fp, "%f", &num); // Leer puntos de datos
            puntos[i+j*(*n)] = num;
        }
    }

    return puntos;
} // ()

 // Crear centroides de manera random
void inicializarCentroides(float *centroides, int k, int d)
{
	int kAux,dAux;

    for(kAux=0; kAux<k; kAux++){
        for(dAux=0; dAux<d; dAux++){
            centroides(kAux, dAux) = (float)(MIN + rand() % ( MAX - MIN + 1 ));
        }
    }
} // ()

 // Importar datos de centroides.txt
float *inicializarCentroidesFichero(int *k, int *d)
{
	int i,j;
    float num; 
	FILE *fp;
	if((fp=fopen("centroides.txt","r"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

	if(fscanf(fp,"K=%d,D=%d\n", k, d)!=2) fprintf(stderr,"load error!\n");
    float *centroides = (float*) malloc((*k)*(*d)*sizeof(float*));
	for(i=0;i<*k;i++){
        for(j=0; j<*d; j++){
            fscanf (fp, "%f", &num); // Leer puntos de datos
            centroides[i+j*(*k)] = num;
        }
    }
    return centroides;
} // ()

 // Mostrar los centroides por pantalla
void printearCentroides(float *centroides, int k, int d){
    int kAux,dAux;

    for(kAux=0; kAux<k; kAux++){
        printf("Centroide %d: ",kAux);
        for(dAux=0; dAux<d; dAux++){
            printf("%f ", centroides(kAux, dAux));
        }
        printf("\n");
    }
} // ()

 // Calcular la distancia euclidiana
float calcularDistancia(float *puntos, float *centroides,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int dAux;
	float sum = 0.0;
    
    for(dAux=0; dAux<d; dAux++){
        sum+=pow(puntos(0,dAux)-centroides(0,dAux),2);
    }
	return sqrt(sum);
} // ()

 // Agrupar N puntos de datos y marcar a qué grupo pertenece cada punto
void asignarPuntosACentroides(float *puntos, float *centroides, int *enCluster,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int nAux,kAux;
	float distanciaMinima;
    float distancia;

	for(nAux=0;nAux<n;nAux++){
		distanciaMinima = 9999.0;
		for(kAux=0;kAux<k;kAux++){
            distancia = calcularDistancia(&puntos(nAux,0), &centroides(kAux,0), n, k, d);
			if(distancia < distanciaMinima){
				distanciaMinima = distancia;
				enCluster[nAux] = kAux;
			}
		}
        //printf ("datos [%d] pertenecen a la clase-%d \n", nAux, enCluster[nAux]);
	}
} // ()

 // Actualiza la variable centroides
void actualizarCentroides(float *puntos, float *centroides, int *enCluster,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	float *centroidesAux = (float*) malloc(k*d*sizeof(float*)); // Almacena cada punto central del grupo
	int nAux, kAux, dAux, count;
    // Inicializar a 0
    for (kAux=0; kAux<k; kAux++){
        for(dAux=0; dAux<d; dAux++){
            centroidesAux(kAux, dAux)=0.0;
        }
    }

	for(kAux=0;kAux<k;kAux++){
		count = 0; // Cuenta todos los puntos de datos que pertenecen a un clúster
		for(nAux=0;nAux<n;nAux++){
			if(enCluster[nAux] == kAux){ // El punto esta en el cluster
                count++; // Hay un punto más
                // Calcula la suma de las dimensiones correspondientes de todos los puntos de datos del clúster
                for(dAux=0; dAux < d; dAux++){
                    centroidesAux(kAux, dAux) += puntos(nAux, dAux);
                }
			}
		}
        for(dAux=0; dAux < d; dAux++){
            centroides(kAux, dAux) = centroidesAux(kAux, dAux)/count;
        }
	}

    free(centroidesAux);
} // ()

 // Calcula la suma de las distancias entre los puntos centrales de todos los grupos y sus puntos de datos
float calcularDiferencia(float *puntos, float *centroides, int *enCluster,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int kAux,nAux;
	float sum=0.0;

	for(kAux = 0; kAux < k; kAux++){
		for(nAux = 0; nAux < n; nAux++){
			if(kAux == enCluster[nAux]) sum += calcularDistancia(&puntos(nAux,0), &centroides(kAux,0), n, k, d);
		}
	}
	return sum;
} // ()

int main(int argc, char *argv[]){
    int n; // Numero de puntos
    int k; // Numero de centroides
    int d; // Numero de ejes de un punto
    
    /* Revisar que recibimos tres parámetros */
    /*if( argc<3 ) {
        printf("Usage: %s n k d\n",argv[0]);
        exit(-1);
    }*/
    //sscanf(argv[1],"%d",&n);
    //sscanf(argv[2],"%d",&k);
    //sscanf(argv[3],"%d",&d);
    
    int i, j, count=0;
    float temp1, temp2 = 0.0;

    double tiempo, ucpu, scpu;
    

    // Seed para que los valores cambien cada ejecución
    //srand (time(NULL));
    //printf("\tInicializando puntos...\n");
    //float *puntos = (float*) malloc(n*d*sizeof(float*));
    //inicializarPuntos(puntos, n, d); // Guardar los puntos (n-puntos)*/
    float *puntos = inicializarPuntosFichero(&n, &d);
    //float *puntos;
    //inicializarPuntos(puntos, &n, &d);
    //printf("\n");
    //printf("\tInicializando enCluster...\n");
    int *enCluster = (int*) malloc(n*sizeof(int*));
    for(i=0; i<n; i++) enCluster[i] = -1;
    //printf("\n");
    // 1.- Inicialización: una vez escogido el número de grupos, k, se establecen k centroides en el espacio de los datos, por ejemplo, escogiéndolos aleatoriamente.
    //printf("\tInicializando centroides...\n");
    //float *centroides = (float*) malloc(k*d*sizeof(float*));
    //inicializarCentroides(centroides, k, d); // Guardar los puntos (n-puntos)*/
    float *centroides = inicializarCentroidesFichero(&k, &d);
    //float *centroides;
    //inicializarCentroides(centroides, &k, &d);
    //printf("\n");
    // 2.- Asignación objetos a los centroides: cada objeto de los datos es asignado a su centroide más cercano.
    //printf("\tAsignando puntos a centroides...\n");

    ctimer( &tiempo, &ucpu, &scpu );
    asignarPuntosACentroides(puntos, centroides, enCluster, n, k, d);
    temp1 = calcularDiferencia(puntos, centroides, enCluster, n, k, d); // La suma de la distancia entre el primer punto central y el punto de datos
	count++;
    //printf("The %dth difference between data and center is: %.2f\n",count,temp1);
    //printf("\n");
    // 3.- Actualización centroides: se actualiza la posición del centroide de cada grupo tomando como nuevo centroide la posición del promedio de los objetos pertenecientes a dicho grupo.
    //printf("\tActualizando centroides...\n");
    actualizarCentroides(puntos, centroides, enCluster, n, k, d);
    //printf("\n");
    while (fabs(temp2-temp1) != 0) {// Compare las dos iteraciones antes y después, si no son iguales, continúe iterando
        temp1 = temp2;
        //printf("\tAsignando puntos a centroides...\n");
        actualizarCentroides(puntos, centroides, enCluster, n, k, d);
        asignarPuntosACentroides(puntos, centroides, enCluster, n, k, d);
        temp2 = calcularDiferencia(puntos, centroides, enCluster, n, k, d);
        count++;
        //printf("The %dth difference between data and center is: %.2f\n",count,temp2);
        //printf("\n");
    }

    ctimer( &tiempo, &ucpu, &scpu );
    printf("n=%d k=%d d=%d\n",n,k,d);
    printf("%f\n",tiempo);

    //printf ("\n\tEl número total de clúster es: %d \n", count); // Cuenta el número de iteraciones
    /*for(i=0; i<k ; i++){
        printf("centroide[%d]: ", i);
        for(j=0; j<d; j++){
            printf("%f ", centroides(i, j));
        }
        printf("\n");
    }*/

    // Liberar memoria
    free(puntos);
    free(enCluster);
    free(centroides);

    return 0;
} // ()
