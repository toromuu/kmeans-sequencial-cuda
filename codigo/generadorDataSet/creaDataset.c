#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[]){
    int n, k, d, i, j, upper_max = 20;

    if (argc != 4){ /* No se han proporcionado los parámetros suficientes. */
        printf("Error: uso del ejecutable: %s n k d\n", argv[0]);
        exit(1);
    }
    n = atoi(argv[1]);
    k = atoi(argv[2]);
    d = atoi(argv[3]);

    srand(time(NULL));
    FILE *fpuntos;
	if((fpuntos=fopen("puntos.txt","w"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

    fprintf(fpuntos, "N=%d,D=%d\n",n,d);

    for(i=0;i<n;i++){
        for(j=0;j<d;j++){
            if(j==(d-1)) {
                fprintf (fpuntos, "%f\n", (float)rand()/(float)(RAND_MAX/upper_max));
            }
            else{
                fprintf (fpuntos, "%f ", (float)rand()/(float)(RAND_MAX/upper_max));
            }
        }
    }
    
    FILE *fcentroides;
	if((fcentroides=fopen("centroides.txt","w"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

    fprintf(fcentroides, "K=%d,D=%d\n",k,d);

    for(i=0;i<k;i++){
        for(j=0;j<d;j++){
            //fprintf (fcentroides, "%f %f\n", (float)rand()/(float)(RAND_MAX/upper_max)+10, (float)rand()/(float)(RAND_MAX/upper_max)+10);
            if(j==(d-1)) {
                fprintf (fcentroides, "%f\n", (float)rand()/(float)(RAND_MAX/upper_max));
            }
            else{
                fprintf (fcentroides, "%f ", (float)rand()/(float)(RAND_MAX/upper_max));
            }
        }
    }
    return 0;
}