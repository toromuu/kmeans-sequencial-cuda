#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "support.h"

#define MAX 5
#define MIN -5
#define nthreads 16

#define	puntos(i,j)		puntos[ (i) + ((j)*(n)) ]
#define	centroides(i,j)		centroides[ (i) + ((j)*(k)) ]
#define	centroidesAux(i,j)		centroidesAux[ (i) + ((j)*(k)) ]
#define d_puntos(i, j) d_puntos[(i) + ((j) * (n))]
#define d_centroides(i, j) d_centroides[(i) + ((j) * (k))]

__device__ float calcularDistanciaCuda(float* d_puntos, float*d_centroides,
                                            int n,
                                            int k,
                                            int d)
{
    int dAux;
	float sum = 0.0;
    
    for(dAux=0; dAux<d; dAux++){
        sum+=pow(d_puntos(0,dAux)-d_centroides(0,dAux),2);
    }
	return sqrt(sum);
}

__global__ void calcularDiferenciaCuda(float*d_puntos, float*d_centroides, int*d_enCluster,
                                            int n,
                                            int k,
                                            int d,
                                            float* d_sum)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < n){
        for(int i = 0; i < k; i++){
            float distancia = 0.0;
            if(i==d_enCluster[idx]){
                distancia = calcularDistanciaCuda(&d_puntos(idx,0), &d_centroides(i,0), n, k, d);
                atomicAdd(d_sum, distancia);         
            }
        }
    } 
}

__global__ void asignarPuntosACentroidesCuda(float* d_puntos, float*d_centroides, int*d_enCluster,
                                                int n,
                                                int k,
                                                int d)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < n){
        float distancia;
        float distanciaMinima = 9999.0;
    
        for(int i=0;i<k;i++){
            distancia = calcularDistanciaCuda(&d_puntos(idx,0), &d_centroides(i,0), n, k, d);
            if(distancia < distanciaMinima){
                distanciaMinima = distancia;
                d_enCluster[idx] = i;
            }
        }
    }
}

__global__ void actualizarCentroidesCuda(float *puntos, int *enCluster,
                                            int n,
                                            int k,
                                            int d,
                                            int *count,
                                            float *centroidesAux)
{
    int nAux = threadIdx.x + blockIdx.x * blockDim.x;
    int kAux = threadIdx.y + blockIdx.y * blockDim.y;
    int dAux;

    if((nAux<n) && (kAux<k)){
        if(enCluster[nAux] == kAux){
            atomicAdd(&count[kAux], 1);
            for(dAux=0; dAux < d; dAux++){
                atomicAdd(&centroidesAux(kAux,dAux), puntos(nAux, dAux));
            }
        }
    }
}

 // Crear puntos de manera random
void inicializarPuntos(float *puntos, int n, int d){
	int nAux,dAux;

    for(nAux=0; nAux<n; nAux++){
        for(dAux=0; dAux<d; dAux++){
            puntos(nAux, dAux) = (float)(MIN + rand() % ( MAX - MIN + 1 ));
        }
    }
} // ()

 // Mostrar los puntos por pantalla
void printearPuntos(float *puntos, int n, int d){
    int nAux,dAux;

    for(nAux=0; nAux<n; nAux++){
        printf("Punto %d: ",nAux);
        for(dAux=0; dAux<d; dAux++){
            printf("%f ", puntos(nAux, dAux));
        }
        printf("\n");
    }
} // ()

 // Importar datos de puntos.txt
float *inicializarPuntosFichero(int *n, int *d) {
	int i,j;
    float num; 
	FILE *fp;
	if((fp=fopen("puntos.txt","r"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

	if(fscanf(fp,"N=%d,D=%d\n",n, d)!=2) fprintf(stderr,"load error!\n");
    float *puntos = (float*) malloc((*n)*(*d)*sizeof(float*));
	for(i=0;i<*n;i++){
        for(j=0; j<*d; j++){
            fscanf (fp, "%f", &num); // Leer puntos de datos
            puntos[i+j*(*n)] = num;
        }
    }

    return puntos;
} // ()

 // Crear centroides de manera random
void inicializarCentroides(float *centroides, int k, int d)
{
	int kAux,dAux;

    for(kAux=0; kAux<k; kAux++){
        for(dAux=0; dAux<d; dAux++){
            centroides(kAux, dAux) = (float)(MIN + rand() % ( MAX - MIN + 1 ));
        }
    }
} // ()

 // Importar datos de centroides.txt
float *inicializarCentroidesFichero(int *k, int *d)
{
	int i,j;
    float num; 
	FILE *fp;
	if((fp=fopen("centroides.txt","r"))==NULL) fprintf(stderr,"No se ha podido abrir el fichero data.txt!\n");

	if(fscanf(fp,"K=%d,D=%d\n", k, d)!=2) fprintf(stderr,"load error!\n");
    float *centroides = (float*) malloc((*k)*(*d)*sizeof(float*));
	for(i=0;i<*k;i++){
        for(j=0; j<*d; j++){
            fscanf (fp, "%f", &num); // Leer puntos de datos
            centroides[i+j*(*k)] = num;
        }
    }
    return centroides;
} // ()

 // Mostrar los centroides por pantalla
void printearCentroides(float *centroides, int k, int d){
    int kAux,dAux;

    for(kAux=0; kAux<k; kAux++){
        printf("Centroide %d: ",kAux);
        for(dAux=0; dAux<d; dAux++){
            printf("%f ", centroides(kAux, dAux));
        }
        printf("\n");
    }
} // ()

 // Calcular la distancia euclidiana
float calcularDistancia(float *puntos, float *centroides,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int dAux;
	float sum = 0.0;
    
    for(dAux=0; dAux<d; dAux++){
        sum+=pow(puntos(0,dAux)-centroides(0,dAux),2);
    }
	return sqrt(sum);
} // ()

 // Agrupar N puntos de datos y marcar a qué grupo pertenece cada punto
void asignarPuntosACentroides(float *puntos, float *centroides, int *enCluster,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int nAux,kAux;
	float distanciaMinima;
    float distancia;

	for(nAux=0;nAux<n;nAux++){
		distanciaMinima = 9999.0;
		for(kAux=0;kAux<k;kAux++){
            distancia = calcularDistancia(&puntos(nAux,0), &centroides(kAux,0), n, k, d);
			if(distancia < distanciaMinima){
				distanciaMinima = distancia;
				enCluster[nAux] = kAux;
			}
		}
        //printf ("datos [%d] pertenecen a la clase-%d \n", nAux, enCluster[nAux]);
	}
} // ()

// Actualiza la variable centroides
void actualizarCentroides(float *puntos, float *centroides, int *enCluster,
    int n, // Numero de puntos
    int k, // Numero de centroides
    int d) // Numero de ejes de un punto
{
    float *centroidesAux = (float*) malloc(k*d*sizeof(float*)); // Almacena cada punto central del grupo
    int *count = (int*) malloc(k*sizeof(int*));
    int kAux, dAux;
    float *d_puntos, *d_centroidesAux;
    int *d_enCluster, *d_count;

    // Reservar memoria en el device
    cudaMalloc((void **) &d_puntos, n*d*sizeof(float));
    cudaMalloc((void **) &d_enCluster, n*sizeof(int));
    cudaMalloc((void **) &d_centroidesAux, k*d*sizeof(float));
    cudaMalloc((void **) &d_count, k*sizeof(int));

    // Inicializar a 0 d_centroidesAux y d_count
    cudaMemset((void**) &d_centroidesAux, 0, k*d*sizeof(float));
    cudaMemset((void**) &d_count, 0, k*sizeof(float));

    // Transferir puntos y enCluster a device
    cudaMemcpy(d_puntos, puntos, n*d*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_enCluster, enCluster, n*sizeof(int), cudaMemcpyHostToDevice);

    // Dimensiones de bloque, columnas y filas
    int blocks_col = (int) ceil( (float) n / (float) nthreads );
    int blocks_row = (int) ceil( (float) k / (float) nthreads );
    // Ejecutar el kernel
    dim3 dimGrid( blocks_col, blocks_row );
    dim3 dimBlock( nthreads, nthreads );
    actualizarCentroidesCuda<<< dimGrid, dimBlock >>>( d_puntos, d_enCluster, n, k, d, d_count, d_centroidesAux );

    // Transferir d_centroidesAux y d_count a host
    cudaMemcpy( centroidesAux, d_centroidesAux, k*d*sizeof(float), cudaMemcpyDeviceToHost );
    cudaMemcpy( count, d_count, k*sizeof(float), cudaMemcpyDeviceToHost );

    for(kAux=0;kAux<k;kAux++){
        //printf("centroide: ");
        for(dAux=0; dAux < d; dAux++){
            centroides(kAux, dAux) = centroidesAux(kAux, dAux)/count[kAux];
            //printf("%f ", centroides(kAux, dAux));
        }
        //printf("\n");
    }

    cudaFree(d_puntos);
    cudaFree(d_centroidesAux);
    cudaFree(d_enCluster);
    cudaFree(d_count);

    free(centroidesAux);
    free(count);
} // ()

 // Calcula la suma de las distancias entre los puntos centrales de todos los grupos y sus puntos de datos
float calcularDiferencia(float *puntos, float *centroides, int *enCluster,
                                int n, // Numero de puntos
                                int k, // Numero de centroides
                                int d) // Numero de ejes de un punto
{
	int kAux,nAux;
	float sum=0.0;

	for(kAux = 0; kAux < k; kAux++){
		for(nAux = 0; nAux < n; nAux++){
			if(kAux == enCluster[nAux]){
                //sum += calcularDistancia(&puntos(nAux,0), &centroides(kAux,0), n, k, d);
                float dist = calcularDistancia(&puntos(nAux,0), &centroides(kAux,0), n, k, d);
                //printf("HOST: %d --> %f\n", nAux, dist);
                sum+=dist;
            } 
		}
	}
	return sum;
} // ()

int main(int argc, char *argv[]){
    int n; // Numero de puntos
    int k; // Numero de centroides
    int d; // Numero de ejes de un punto
    Timer timer;
    
    /* Revisar que recibimos tres parámetros */
    /*
    if( argc<3 ) {
        printf("Usage: %s n k d\n",argv[0]);
        exit(-1);
    }
    sscanf(argv[1],"%d",&n);
    sscanf(argv[2],"%d",&k);
    sscanf(argv[3],"%d",&d);
    */
    int i, j, count=0;
    float temp1, temp2 = 0.0;
    // Seed para que los valores cambien cada ejecución
    //srand (time(NULL));
    //printf("\tInicializando puntos...\n");
    /*float *puntos = (float*) malloc(n*d*sizeof(float*));
    inicializarPuntos(puntos, n, d); // Guardar los puntos (n-puntos)*/
    float *puntos = inicializarPuntosFichero(&n, &d);
    //printf("\n");
    //printf("\tInicializando enCluster...\n");
    int *enCluster = (int*) malloc(n*sizeof(int*));
    for(i=0; i<n; i++) enCluster[i] = -1;
    //printf("\n");
    // 1.- Inicialización: una vez escogido el número de grupos, k, se establecen k centroides en el espacio de los datos, por ejemplo, escogiéndolos aleatoriamente.
    //printf("\tInicializando centroides...\n");
    /*float *centroides = (float*) malloc(k*d*sizeof(float*));
    inicializarCentroides(centroides, k, d); // Guardar los puntos (n-puntos)*/
    float *centroides = inicializarCentroidesFichero(&k, &d);
    //printf("\n");

    // Inicializar variables device
    float *d_puntos = 0;
    cudaMalloc((void**)&d_puntos, n * d * sizeof(float));         // almacenar datos
    int *d_enCluster = 0;
    cudaMalloc((void**)&d_enCluster, n * sizeof(int));         // Marque a qué grupo pertenece cada punto
    float *d_centroides = 0;
    cudaMalloc((void**)&d_centroides, k * d * sizeof(float)); // Almacena el punto central de cada grupo
    float* d_temp1 = 0;
    cudaMalloc((void**)&d_temp1, sizeof(float));
    float* d_temp2 = 0;
    cudaMalloc((void**)&d_temp2, sizeof(float));

    // Copiar valores de host a device
    cudaMemcpy(d_centroides, centroides, k * d * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_puntos, puntos, n * d * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_enCluster, enCluster, n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_temp1, &temp1, sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_temp2, &temp2, sizeof(float), cudaMemcpyHostToDevice);

    // 2.- Asignación objetos a los centroides: cada objeto de los datos es asignado a su centroide más cercano.
    //printf("\tAsignando puntos a centroides...\n");

    startTime(&timer);
    //asignarPuntosACentroides(puntos, centroides, enCluster, n, k, d);
    asignarPuntosACentroidesCuda<<<ceil(float(n)/nthreads), nthreads>>>(d_puntos, d_centroides, d_enCluster, n, k, d);
    cudaMemcpy(enCluster, d_enCluster, n * sizeof(int), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    
    calcularDiferenciaCuda<<<ceil(float(n)/nthreads), nthreads>>>(d_puntos, d_centroides, d_enCluster, n, k, d, d_temp1);
    cudaMemcpy(&temp1, d_temp1, sizeof(float), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
	count++;

    //printf("\n");
    //printf("The %dth difference between data and center is: %.2f\n",count,temp1);
    //printf("\n");
    // 3.- Actualización centroides: se actualiza la posición del centroide de cada grupo tomando como nuevo centroide la posición del promedio de los objetos pertenecientes a dicho grupo.
    //printf("\tActualizando centroides...\n");

    actualizarCentroides(puntos, centroides, enCluster, n, k, d);
    cudaMemcpy(d_centroides, centroides, n * d * sizeof(float), cudaMemcpyHostToDevice);

    //printf("\n");
    while (fabs(temp2-temp1) != 0) {// Compare las dos iteraciones antes y después, si no son iguales, continúe iterando
        temp1 = temp2;
        temp2 = 0;
        cudaMemcpy(d_temp1, &temp1, sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(d_temp2, &temp2, sizeof(float), cudaMemcpyHostToDevice);

        //printf("\tAsignando puntos a centroides...\n");

        actualizarCentroides(puntos, centroides, enCluster, n, k, d);
        asignarPuntosACentroidesCuda<<<ceil(float(n)/nthreads), nthreads>>>(d_puntos, d_centroides, d_enCluster, n, k, d);
        cudaMemcpy(&temp1, d_temp1, sizeof(float), cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();

        cudaMemcpy(d_centroides, centroides, k * d * sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(d_puntos, puntos, n * d * sizeof(float), cudaMemcpyHostToDevice);

        calcularDiferenciaCuda<<<ceil(float(n)/nthreads), nthreads>>>(d_puntos, d_centroides, d_enCluster, n, k, d, d_temp2);
        cudaMemcpy(&temp2, d_temp2, sizeof(float), cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();

        count++;
        //printf("The %dth difference between data and center is: %.2f\n",count,temp2);
        //printf("\n");
    }
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("n=%d k=%d d=%d\n",n,k,d);
    /*
    printf ("\n\tEl número total de clúster es: %d \n", count); // Cuenta el número de iteraciones
    for(i=0; i<k ; i++){
        printf("centroide[%d]: ", i);
        for(j=0; j<d; j++){
            printf("%f ", centroides(i, j));
        }
        printf("\n");
    }*/

    // Liberar memoria
    free(puntos);
    free(enCluster);
    free(centroides);

    // considerar cuda free ?
    //cudaFree(centroidesAux);



    return 0;
} // ()
