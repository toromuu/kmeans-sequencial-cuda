#!/bin/bash


# var k --> cte n,d

for n in 5000
do
    for k in 16 64 256 1024 2048
    do
        for d in 2
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Kvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Kvar.txt
            mv centroides.txt ./dataset/centroides/Kvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Kvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done


