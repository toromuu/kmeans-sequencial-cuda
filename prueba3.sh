#!/bin/bash


# var d --> cte n,k

for n in 5000
do
    for k in 16
    do
        for d in 2 4 8 16 32
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Dvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Dvar.txt
            mv centroides.txt ./dataset/centroides/Dvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Dvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done


