#!/bin/bash

# PRUEBA 1
# var n --> cte k,d

for n in 500 5000 50000 500000 1000000
do
    for k in 16
    do
        for d in 2
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Nvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Nvar.txt
            mv centroides.txt ./dataset/centroides/Nvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Nvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done

# PRUEBA 2
# var k --> cte n,d

for n in 5000
do
    for k in 16 64 256 1024 2048
    do
        for d in 2
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Kvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Kvar.txt
            mv centroides.txt ./dataset/centroides/Kvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Kvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done

# PRUEBA 3
# var d --> cte n,k

for n in 5000
do
    for k in 16
    do
        for d in 2 4 8 16 32
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial_Dvar.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda_Dvar.txt
            mv centroides.txt ./dataset/centroides/Dvar/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/Dvar/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done

# PRUEBA 4
# var n,k,d --> cte n,k

for n in 5000 50000
do
    for k in 64 256
    do
        for d in 4 8
        do
            ./creaDataset $n $k $d
            ./main_Secuencial >> ./resultados/Secuencial/Secuencial.txt
            ./main_Cuda >> ./resultados/Cuda/Cuda.txt
            mv centroides.txt ./dataset/centroides/centroides_${n}n_${k}k_${d}d.txt
            mv puntos.txt ./dataset/puntos/puntos_${n}n_${k}k_${d}d.txt
        done
    done
done


